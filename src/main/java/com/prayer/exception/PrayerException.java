package com.prayer.exception;

public class PrayerException extends Exception {
    private int    code;
    private String message;

    public PrayerException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}