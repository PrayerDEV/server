package com.prayer.exception;

import com.prayer.constant.ErrorCode;
import com.prayer.model.response.error.FieldErrorData;
import com.prayer.model.response.error.PrayerErrorData;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice(annotations = {RestController.class})
public class GlobalExceptionController {
    @ExceptionHandler(value = FieldException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public FieldErrorData handleFieldException(FieldException ex) {
        FieldErrorData errorData = new FieldErrorData();
        errorData.setFieldErrorList(ex.getErrorList());
        errorData.setCode(ErrorCode.INVALID_PARAMETERS);
        errorData.setType(ex.getClass().getSimpleName());
        return errorData;
    }

    @ExceptionHandler(value = PrayerException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PrayerErrorData handlePrayerException(PrayerException ex) {
        PrayerErrorData errorData = new PrayerErrorData<String>();
        errorData.setCode(ex.getCode());
        errorData.setType(ex.getClass().getSimpleName());
        errorData.setMessage(ex.getMessage());
        return errorData;
    }
}
