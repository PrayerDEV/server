package com.prayer.exception;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

public class FieldException extends Exception {
    private List<ObjectError> errorList;

    public FieldException(BindingResult result) {
        this.errorList = result.getAllErrors();
    }

    public List<ObjectError> getErrorList() {
        return errorList;
    }
}
