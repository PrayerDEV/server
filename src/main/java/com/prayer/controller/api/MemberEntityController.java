package com.prayer.controller.api;

import com.prayer.constant.ErrorCode;
import com.prayer.constant.ParamName;
import com.prayer.constant.ParamTitle;
import com.prayer.exception.FieldException;
import com.prayer.exception.PrayerException;
import com.prayer.init.SwaggerConfig;
import com.prayer.model.MemberEntity;
import com.prayer.model.request.LoginRequestData;
import com.prayer.model.request.RegisterRequestData;
import com.prayer.model.response.LoginResponseData;
import com.prayer.model.response.RegisterResponseData;
import com.prayer.service.MemberEntityService;
import com.wordnik.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;


@RestController
@RequestMapping(value = "v1/member")
@Api(value = "회원관리", description = "회원관리 API", basePath = "/member")
public class MemberEntityController {
    @Autowired
    private MemberEntityService memberEntityService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation(
            value = "회원가입",
            notes = "회원가입 API")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.MEMBER_DUPLICATE, message = "이미 가입된 사용자")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = ParamName.MEMBER_ID         , value = ParamTitle.MEMBER_ID         , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.NAME              , value = ParamTitle.NAME              , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.PASSWORD          , value = ParamTitle.PASSWORD          , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.EMAIL             , value = ParamTitle.EMAIL             , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.OPEN              , value = ParamTitle.OPEN              , required = true, dataType = "byte"  , paramType = "query"),
            @ApiImplicitParam(name = ParamName.UUID              , value = ParamTitle.UUID              , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.NOTIFICATION_TOKEN, value = ParamTitle.NOTIFICATION_TOKEN, required = true, dataType = "string", paramType = "query")
    })
    public RegisterResponseData create(@Valid RegisterRequestData requestData, BindingResult result) throws FieldException, PrayerException {
        if (result.hasErrors()) {
            throw new FieldException(result);
        }
        return memberEntityService.register(requestData);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(
            value = "로그인",
            notes = "로그인 API")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.MEMBER_MISMATCH_ID, message = "아이디 에러"),
            @ApiResponse(code = ErrorCode.MEMBER_MISMATCH_PW, message = "패스워드 에러")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = ParamName.MEMBER_ID, value = ParamTitle.MEMBER_ID, required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.PASSWORD , value = ParamTitle.PASSWORD , required = true, dataType = "string", paramType = "query")
    })
    public LoginResponseData login(@Valid LoginRequestData requestData, BindingResult result) throws FieldException, PrayerException {
        if (result.hasErrors()) {
            throw new FieldException(result);
        }
        return memberEntityService.login(requestData);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(
            value = "회원목록",
            notes = "회원목록 API",
            authorizations = {@Authorization(value = SwaggerConfig.SECURITY_SCHEMA_OAUTH2, type = "oauth2", scopes = {@AuthorizationScope( scope = SwaggerConfig.AUTHORIZATION_SCOPE_GLOBAL, description = SwaggerConfig.AUTHORIZATION_SCOPE_GLOBAL_DESC)})})
    public Page<MemberEntity> list(@ApiParam(name = ParamName.OFFSET, value = ParamTitle.OFFSET, defaultValue = "0"  , required = true) @RequestParam(value = ParamName.OFFSET, defaultValue = "0"  , required = true) Integer offset,
                                   @ApiParam(name = ParamName.LIMIT , value = ParamTitle.LIMIT , defaultValue = "100", required = true) @RequestParam(value = ParamName.LIMIT , defaultValue = "100", required = true) Integer limit) {
        return memberEntityService.findAll(offset, limit);
    }

    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    @ApiOperation(
            value = "회원정보",
            notes = "회원정보 API")
    public MemberEntity info(@ApiParam(name = ParamName.MEMBER_ID, value = ParamTitle.MEMBER_ID, required = true) @PathVariable @Size(min = 1) Integer id) {
        return memberEntityService.findById(id);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT)
    @ApiOperation(
            value = "회원정보 수정",
            notes = "회원정보 수정 API")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.MEMBER_NOT_EXIST, message = "존재하지 않는 사용자")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = ParamName.NAME              , value = ParamTitle.NAME              , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.PASSWORD          , value = ParamTitle.PASSWORD          , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.EMAIL             , value = ParamTitle.EMAIL             , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.OPEN              , value = ParamTitle.OPEN              , required = true, dataType = "byte"  , paramType = "query"),
            @ApiImplicitParam(name = ParamName.UUID              , value = ParamTitle.UUID              , required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = ParamName.NOTIFICATION_TOKEN, value = ParamTitle.NOTIFICATION_TOKEN, required = true, dataType = "string", paramType = "query")
    })
    public MemberEntity edit(MemberEntity memberEntity, @PathVariable @Size(min = 1) Integer id) throws PrayerException {
        return memberEntityService.update(memberEntity, id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ApiOperation(
            value = "회원 삭제",
            notes = "회원 삭제 API")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.MEMBER_NOT_EXIST, message = "존재하지 않는 사용자")
    })
    public MemberEntity delete(@ApiParam(name = ParamName.MEMBER_ID, value = ParamTitle.MEMBER_ID, required = true) @PathVariable @Size(min = 1) Integer id) throws PrayerException {
        return memberEntityService.delete(id);
    }
}
