package com.prayer.controller.web;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Api(value = "웹 페이지", description = "웹 페이지 링크", basePath = "/")
public class WebController {
	@RequestMapping(value={"/"}, method=RequestMethod.GET)
	@ApiOperation(value = "루트 페이지", notes = "루트 페이지")
	public String root(ModelMap model) {
		model.addAttribute("menu", "info");
		return "index";
	}
}
