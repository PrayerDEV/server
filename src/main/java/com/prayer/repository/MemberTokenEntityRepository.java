package com.prayer.repository;

import com.prayer.model.MemberTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberTokenEntityRepository extends JpaRepository<MemberTokenEntity, Integer>{
}



