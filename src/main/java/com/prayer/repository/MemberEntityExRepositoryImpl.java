package com.prayer.repository;

import com.prayer.model.MemberEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MemberEntityExRepositoryImpl implements MemberEntityExRepository {

    @PersistenceContext
    EntityManager em;

    @Override
    public MemberEntity findByMbId(String mbid) {
        return (MemberEntity)em.createNativeQuery("SELECT m FROM member m WHERE m.mb_id = :mbid",MemberEntity.class)
                .setParameter("mbid", mbid)
                .getResultList();
    }

    @Override
    public MemberEntity findByMbIdAndPassword(String mbid, String password) {
        return (MemberEntity)em.createNativeQuery("SELECT m FROM member m WHERE m.mb_id = :mbid AND m.password = :password", MemberEntity.class)
                .setParameter("mbid", mbid)
                .setParameter("password", password)
                .getResultList();
    }
}
