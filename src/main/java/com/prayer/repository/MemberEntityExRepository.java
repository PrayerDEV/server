package com.prayer.repository;

import com.prayer.model.MemberEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberEntityExRepository {
    MemberEntity findByMbId(String mbId);
    MemberEntity findByMbIdAndPassword(String mbId, String pw);
}
