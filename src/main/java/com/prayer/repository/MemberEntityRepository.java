package com.prayer.repository;

import com.prayer.model.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberEntityRepository extends JpaRepository<MemberEntity, Integer>, MemberEntityExRepository {
}
