package com.prayer.service;


import com.prayer.constant.ErrorCode;
import com.prayer.exception.PrayerException;
import com.prayer.model.response.RegisterResponseData;
import com.prayer.util.SecureRandomGenerator;
import org.apache.commons.codec.binary.Base64;
import com.prayer.constant.ErrorMesage;
import com.prayer.exception.FieldException;
import com.prayer.model.MemberEntity;
import com.prayer.model.MemberTokenEntity;
import com.prayer.model.request.LoginRequestData;
import com.prayer.model.request.RegisterRequestData;
import com.prayer.model.response.LoginResponseData;
import com.prayer.repository.MemberEntityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class MemberEntityServiceImpl implements MemberEntityService {
    @Resource
    private MemberEntityRepository memberEntityRepository;

    @Override
    @Transactional
    public RegisterResponseData register(RegisterRequestData requestData) throws FieldException, PrayerException {
        if (memberEntityRepository.findByMbId(requestData.getMbId()) != null) {
            throw new PrayerException(ErrorCode.MEMBER_DUPLICATE, ErrorMesage.MEMBER_DUPLICATE);
        }

        Date accessTokenExpireDate = new Date();
        String accessToken = getAccessToken(requestData.getName(), accessTokenExpireDate);

        Date refreshTokenExpireDate = getAfterMonthDate();
        String refreshToken = getRefreshToken(requestData.getPassword(), refreshTokenExpireDate);

        MemberEntity memberEntity = createMemberEntity(requestData, accessToken, accessTokenExpireDate, refreshToken, refreshTokenExpireDate);
        memberEntityRepository.save(memberEntity);
        return createRegisterResponseData(accessToken, refreshToken);
    }

    private String getAccessToken(String name, Date expireDate) {
        StringBuffer keySource = new StringBuffer();
        keySource.append(name);
        keySource.append(expireDate.toString());
        keySource.append(new SecureRandomGenerator().getSecureRandomString());
        return new String(new Base64(true).encodeBase64(keySource.toString().getBytes()));
    }

    private String getRefreshToken(String password, Date expireDate) {
        StringBuffer nextKeySource = new StringBuffer();
        nextKeySource.append(password);
        nextKeySource.append(expireDate.toString());
        nextKeySource.append(new SecureRandomGenerator().getSecureNextRandomString());
        return new String(new Base64(true).encodeBase64(nextKeySource.toString().getBytes()));
    }

    private Date getAfterMonthDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }

    private MemberEntity createMemberEntity(RegisterRequestData requestData, String accessToken, Date accessTokenExpireDate, String refreshToken, Date refreshTokenExpireDate) {
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setMbId(requestData.getMbId());
        memberEntity.setName(requestData.getName());
        memberEntity.setPassword(BCrypt.hashpw(requestData.getPassword(), BCrypt.gensalt(12)));
        memberEntity.setEmail(requestData.getEmail());

        MemberTokenEntity memberTokenEntity = new MemberTokenEntity();
        memberTokenEntity.setNotificationToken(requestData.getNotificationToken());
        memberTokenEntity.setDeviceUuid(requestData.getUuid());
        memberTokenEntity.setAccessToken(accessToken);
        memberTokenEntity.setAccessTokenExpires(accessTokenExpireDate);
        memberTokenEntity.setRefreshToken(refreshToken);
        memberTokenEntity.setRefreshTokenExpires(refreshTokenExpireDate);

        List<MemberTokenEntity> memberTokenEntities = new ArrayList<MemberTokenEntity>();
        memberTokenEntities.add(memberTokenEntity);

        memberEntity.setMemberTokenEntities(memberTokenEntities);
        return memberEntity;
    }

    private RegisterResponseData createRegisterResponseData(String accessToken, String refreshToken) {
        RegisterResponseData responseData = new RegisterResponseData();
        responseData.setAccessToken(accessToken);
        responseData.setRefreshToken(refreshToken);
        return responseData;
    }

    @Override
    @Transactional
    public MemberEntity findById(int id) {
        return memberEntityRepository.findOne(id);
    }

    @Override
    @Transactional
    public LoginResponseData login(LoginRequestData requestData) throws FieldException, PrayerException {
        MemberEntity memberEntity = memberEntityRepository.findByMbId(requestData.getMbId());
        checkMember(memberEntity, requestData.getPassword());
        MemberTokenEntity memberTokenEntity = memberEntity.getMemberTokenEntities().get(0);
        return createLoginResponseData(memberTokenEntity.getAccessToken(), memberTokenEntity.getRefreshToken());
    }

    private void checkMember(MemberEntity memberEntity, String inputPassword) throws PrayerException {
        String password;
        if (memberEntity == null) {
            throw new PrayerException(ErrorCode.MEMBER_MISMATCH_ID, ErrorMesage.MEMBER_MISMATCH_ID);
        }else{
            password = memberEntity.getPassword();
        }

        if (!BCrypt.checkpw(inputPassword, password)) {
            throw new PrayerException(ErrorCode.MEMBER_MISMATCH_PW, ErrorMesage.MEMBER_MISMATCH_PW);
        }
    }

    private LoginResponseData createLoginResponseData(String accessToken, String refreshToken) {
        LoginResponseData responseData = new LoginResponseData();
        responseData.setAccessToken(accessToken);
        responseData.setRefreshToken(refreshToken);
        return responseData;
    }


    @Override
    @Transactional(rollbackFor = PrayerException.class)
    public MemberEntity delete(int id) throws PrayerException {
        MemberEntity deletedMembers = memberEntityRepository.findOne(id);
        if (deletedMembers == null) {
            throw new PrayerException(ErrorCode.MEMBER_NOT_EXIST, ErrorMesage.MEMBER_NOT_EXIST);
        }
        memberEntityRepository.delete(deletedMembers);
        return deletedMembers;
    }

    @Override
    public Page<MemberEntity> findAll(int offset, int limit) {
        return memberEntityRepository.findAll(new PageRequest(offset, limit));
    }

    @Override
    @Transactional(rollbackFor = PrayerException.class)
    public MemberEntity update(MemberEntity memberEntity, int id) throws PrayerException {
        MemberEntity updatedMemberEntity = memberEntityRepository.findOne(id);
        if (updatedMemberEntity == null) {
            throw new PrayerException(ErrorCode.MEMBER_NOT_EXIST, ErrorMesage.MEMBER_NOT_EXIST);
        }
        return updatedMemberEntity;
    }
}
