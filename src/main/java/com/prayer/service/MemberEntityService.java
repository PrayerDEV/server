package com.prayer.service;

import com.prayer.exception.PrayerException;
import com.prayer.model.MemberEntity;
import com.prayer.model.request.LoginRequestData;
import com.prayer.model.request.RegisterRequestData;
import com.prayer.model.response.LoginResponseData;
import com.prayer.model.response.RegisterResponseData;
import com.prayer.exception.FieldException;
import org.springframework.data.domain.Page;

public interface MemberEntityService {
    RegisterResponseData register(RegisterRequestData requestData) throws FieldException, PrayerException;

    LoginResponseData login(LoginRequestData requestData) throws FieldException, PrayerException;

    MemberEntity delete(int id) throws PrayerException;

    Page<MemberEntity> findAll(int offset, int limit);

    MemberEntity update(MemberEntity memberEntity, int id) throws PrayerException;

    MemberEntity findById(int id);

}
