package com.prayer.util;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by ward on 2015. 11. 27..
 */
public class SecureRandomGenerator {
    private SecureRandom random = new SecureRandom();

    public String getSecureRandomString(){
        return new BigInteger(130, random).toString(32);
    }

    public String getSecureNextRandomString(){

        byte bytes[] = new byte[20];
        random.nextBytes(bytes);

        return random.toString();
    }
}
