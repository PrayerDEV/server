package com.prayer.model;

import javax.persistence.PrePersist;
import java.util.Date;

public class CreatedAtListener {
    @PrePersist
    public void setCreatedAt(final Creatable entity){
        entity.setCreatedAt(new Date());
    }
}
