package com.prayer.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginRequestData {
    @NotNull
    @Size(min = 2, max = 5)
    private String mbId;

    @NotNull
    @Size(min = 8, max = 15)
    private String password;

    public String getMbId() {
        return mbId;
    }

    public void setMbId(String mbId) {
        this.mbId = mbId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
