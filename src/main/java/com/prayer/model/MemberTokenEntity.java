package com.prayer.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "member_token", schema = "prayerdb", catalog = "")
public class MemberTokenEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "member_id",insertable = false, updatable = false)
    private Integer memberId;

    @Column(name = "notification_token")
    private String notificationToken;

    @Column(name = "device_uuid")
    private String deviceUuid;

    @Column(name = "access_token")
    private String accessToken;

    @Column(name = "access_token_expires")
    private Date accessTokenExpires;

    @Column(name = "refresh_token")
    private String refreshToken;

    @Column(name = "refresh_token_expires")
    private Date refreshTokenExpires;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }


    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }


    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


    public Date getAccessTokenExpires() {
        return accessTokenExpires;
    }

    public void setAccessTokenExpires(Date accessTokenExpires) {
        this.accessTokenExpires = accessTokenExpires;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Date getRefreshTokenExpires() {
        return refreshTokenExpires;
    }

    public void setRefreshTokenExpires(Date refreshTokenExpires) {
        this.refreshTokenExpires = refreshTokenExpires;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MemberTokenEntity that = (MemberTokenEntity) o;

        if (id != that.id) return false;
        if (memberId != that.memberId) return false;
        if (notificationToken != null ? !notificationToken.equals(that.notificationToken) : that.notificationToken != null)
            return false;
        if (deviceUuid != null ? !deviceUuid.equals(that.deviceUuid) : that.deviceUuid != null) return false;
        if (accessToken != null ? !accessToken.equals(that.accessToken) : that.accessToken != null) return false;
        if (accessTokenExpires != null ? !accessTokenExpires.equals(that.accessTokenExpires) : that.accessTokenExpires != null)
            return false;
        if (refreshToken != null ? !refreshToken.equals(that.refreshToken) : that.refreshToken != null) return false;
        if (refreshTokenExpires != null ? !refreshTokenExpires.equals(that.refreshTokenExpires) : that.refreshTokenExpires != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + memberId;
        result = 31 * result + (notificationToken != null ? notificationToken.hashCode() : 0);
        result = 31 * result + (deviceUuid != null ? deviceUuid.hashCode() : 0);
        result = 31 * result + (accessToken != null ? accessToken.hashCode() : 0);
        result = 31 * result + (accessTokenExpires != null ? accessTokenExpires.hashCode() : 0);
        result = 31 * result + (refreshToken != null ? refreshToken.hashCode() : 0);
        result = 31 * result + (refreshTokenExpires != null ? refreshTokenExpires.hashCode() : 0);
        return result;
    }
}
