package com.prayer.model;

import java.util.Date;

public interface Creatable {
    void setCreatedAt(final Date date);
}
