package com.prayer.model;

import javax.persistence.PrePersist;
import java.util.Date;

public class ModifiedAtListener {
    @PrePersist
    public void setModifiedAt(final Modifiable entity){
        entity.setModifiedAt(new Date());
    }
}
