package com.prayer.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@EntityListeners({ CreatedAtListener.class, ModifiedAtListener.class })
@Table(name = "member")
public class MemberEntity implements Creatable, Modifiable{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "mb_id")
    private String mbId;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "profile")
    private String profile;

    @Column(name = "level")
    private byte level;

    @Column(name = "leave_date")
    private String leaveDate;

    @Column(name = "intercept_date")
    private String interceptDate;

    @Column(name = "mailling")
    private Byte mailling;


    @Column(name = "open")
    private byte open;

    @Column(name = "open_date")
    private Date openDate;

    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_date")
    private Date createdDate;


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "member_id", nullable = false)
    private List<MemberTokenEntity> memberTokenEntities = new ArrayList<MemberTokenEntity>();

    public List<MemberTokenEntity> getMemberTokenEntities() {
        return memberTokenEntities;
    }

    public void setMemberTokenEntities(List<MemberTokenEntity> memberTokenEntities) {
        this.memberTokenEntities = memberTokenEntities;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getMbId() {
        return mbId;
    }

    public void setMbId(String mbId) {
        this.mbId = mbId;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }


    public byte getLevel() {
        return level;
    }

    public void setLevel(byte level) {
        this.level = level;
    }



    public String getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }



    public String getInterceptDate() {
        return interceptDate;
    }

    public void setInterceptDate(String interceptDate) {
        this.interceptDate = interceptDate;
    }



    public Byte getMailling() {
        return mailling;
    }

    public void setMailling(Byte mailling) {
        this.mailling = mailling;
    }



    public byte getOpen() {
        return open;
    }

    public void setOpen(byte open) {
        this.open = open;
    }



    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }



    public java.util.Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(java.util.Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }



    public java.util.Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(java.util.Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MemberEntity that = (MemberEntity) o;

        if (id != that.id) return false;
        if (level != that.level) return false;
        if (open != that.open) return false;
        if (mbId != null ? !mbId.equals(that.mbId) : that.mbId != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (profile != null ? !profile.equals(that.profile) : that.profile != null) return false;
        if (leaveDate != null ? !leaveDate.equals(that.leaveDate) : that.leaveDate != null) return false;
        if (interceptDate != null ? !interceptDate.equals(that.interceptDate) : that.interceptDate != null)
            return false;
        if (mailling != null ? !mailling.equals(that.mailling) : that.mailling != null) return false;
        if (openDate != null ? !openDate.equals(that.openDate) : that.openDate != null) return false;
        if (modifiedDate != null ? !modifiedDate.equals(that.modifiedDate) : that.modifiedDate != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (mbId != null ? mbId.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (profile != null ? profile.hashCode() : 0);
        result = 31 * result + (int) level;
        result = 31 * result + (leaveDate != null ? leaveDate.hashCode() : 0);
        result = 31 * result + (interceptDate != null ? interceptDate.hashCode() : 0);
        result = 31 * result + (mailling != null ? mailling.hashCode() : 0);
        result = 31 * result + (int) open;
        result = 31 * result + (openDate != null ? openDate.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        return result;
    }

    @Override
    public void setCreatedAt(Date date) {
        createdDate = date;
        openDate = date;
    }

    @Override
    public void setModifiedAt(Date date) {
        modifiedDate = date;
    }
}
