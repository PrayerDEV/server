package com.prayer.model;

import java.util.Date;

public interface Modifiable {
    void setModifiedAt(final Date date);
}
