package com.prayer.model.response;

public class LoginResponseData extends ResponseData {
    private String accessToken;
    private String refreshToken;

    public LoginResponseData() {
        super(true);
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
