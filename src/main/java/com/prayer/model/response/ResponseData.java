package com.prayer.model.response;

public class ResponseData {
    private boolean result;

    public ResponseData(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }
}
