package com.prayer.model.response.error;


import com.prayer.model.response.ResponseData;

public abstract class ErrorData<T> extends ResponseData {
    private int    code;
    private String type;
    private T      message;

    public ErrorData() {
        super(false);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}


