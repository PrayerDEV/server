package com.prayer.constant;

public interface ErrorCode {
    int MEMBER_NOT_EXIST = 2000;
    int MEMBER_DUPLICATE = 2001;
    int MEMBER_MISMATCH_ID = 2002;
    int MEMBER_MISMATCH_PW = 2003;

    int FILE_SAVE_STORAGE = 3000;

    int INVALID_PARAMETERS = 6001;
}
