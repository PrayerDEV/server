package com.prayer.constant;

public interface ErrorMesage {
    String MEMBER_NOT_EXIST   = "error.member.exist";
    String MEMBER_DUPLICATE   = "error.member.duplicate";
    String MEMBER_MISMATCH_ID = "error.member.mismatch.id";
    String MEMBER_MISMATCH_PW = "error.member.mismatch.pw";
    String FILE_SAVE_STORAGE  = "error.save.storage";
    String INVALID_PARAMETERS = "error.invalid.parameters";

}
