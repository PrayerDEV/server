package com.prayer.constant;

public interface ParamTitle {
    String MEMBER_ID          = "회원 아이디";
    String NAME               = "회원 이름";
    String PASSWORD           = "비밀번호";
    String EMAIL              = "이메일 주소";
    String OPEN               = "공개여부 (1 : 공개, 0 : 비공개";
    String UUID               = "UUID";
    String NOTIFICATION_TOKEN = "노티 토큰";
    String RECEIVE_MEMBER_ID  = "받는사람 아이디";
    String SEND_MEMBER_ID     = "보낸사람 아이디";
    String MEMO               = "메모내용";
    String ID                 = "아이디";
    String IS_READ            = "읽음여부";
    String OFFSET             = "오프셋";
    String LIMIT              = "리미트";
}