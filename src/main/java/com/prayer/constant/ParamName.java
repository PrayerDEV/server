package com.prayer.constant;

public interface ParamName {
    String MEMBER_ID          = "mbId";
    String NAME               = "name";
    String PASSWORD           = "password";
    String EMAIL              = "email";
    String OPEN               = "open";
    String UUID               = "uuid";
    String NOTIFICATION_TOKEN = "notificationToken";
    String RECEIVE_MEMBER_ID  = "recvMbId";
    String SEND_MEMBER_ID     = "sendMbId";
    String MEMO               = "memo";
    String ID                 = "id";
    String IS_READ            = "isRead";
    String OFFSET             = "offset";
    String LIMIT              = "limit";
}
