package com.prayer.init;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    public static final String SECURITY_SCHEMA_OAUTH2          = "oauth2schema";
    public static final String AUTHORIZATION_SCOPE_GLOBAL      = "global";
    public static final String AUTHORIZATION_SCOPE_GLOBAL_DESC = "accessEverything";

    @Bean
    public Docket apiDocumentation() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("api").apiInfo(apiInfo())
                .select().paths(PathSelectors.any()).build()
                .securitySchemes(newArrayList(securitySchema()))
                .securityContexts(newArrayList(securityContext()));
    }

    private OAuth securitySchema() {
        AuthorizationScope authorizationScope = new AuthorizationScope(AUTHORIZATION_SCOPE_GLOBAL, AUTHORIZATION_SCOPE_GLOBAL);
        LoginEndpoint loginEndpoint = new LoginEndpoint("http://localhost:8080/sso/login");
        GrantType grantType = new ImplicitGrant(loginEndpoint, "access_token");
        return new OAuth(SECURITY_SCHEMA_OAUTH2, newArrayList(authorizationScope), newArrayList(grantType));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "Prayer API",
                "Prayer Developer API",
                "1.0",
                "https://prayer.com/terms/",
                "contact@prayer.com",
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0.html"
        );
        return apiInfo;
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[]{new AuthorizationScope(AUTHORIZATION_SCOPE_GLOBAL, AUTHORIZATION_SCOPE_GLOBAL_DESC)};
        return newArrayList(new SecurityReference(SECURITY_SCHEMA_OAUTH2, authorizationScopes));
    }
}
