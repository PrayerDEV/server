package com.prayer.init;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class DataConfig {
    private static final String PROPERTY_NAME_DATABASE_DRIVER 				 = "db.driver";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD 			 = "db.password";
    private static final String PROPERTY_NAME_DATABASE_URL 					 = "db.url";
    private static final String PROPERTY_NAME_DATABASE_USERNAME 			 = "db.username";
    private static final String PROPERTY_NAME_HIBERNATE_DIALECT 			 = "hibernate.dialect";
    private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL 			 = "hibernate.show_sql";
    private static final String PROPERTY_NAME_HIBERNATE_ID 			         = "hibernate.id.new_generator_mappings";
    private static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";

    @Resource
    private Environment environment;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setPackagesToScan(environment.getRequiredProperty(PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
        entityManagerFactoryBean.setJpaProperties(hibProperties());
        return entityManagerFactoryBean;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
        dataSource.setUrl(environment.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));
        dataSource.setUsername(environment.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        initUsersTable(jdbcTemplate);
        return dataSource;
    }

    private void initUsersTable(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS member (" +
                "seq int(11) NOT NULL AUTO_INCREMENT," +
                "mb_id varchar(255) NOT NULL DEFAULT ''," +
                "password varchar(255) NOT NULL DEFAULT ''," +
                "name varchar(255) NOT NULL DEFAULT ''," +
                "email varchar(255) NOT NULL DEFAULT ''," +
                "profile varchar(255) DEFAULT ''," +
                "level tinyint(4) NOT NULL DEFAULT '0'," +
                "leave_date varchar(8) DEFAULT ''," +
                "intercept_date varchar(8) DEFAULT ''," +
                "mailling tinyint(4) DEFAULT '0'," +
                "open tinyint(4) NOT NULL DEFAULT '0'," +
                "open_date date NOT NULL," +
                "modified_date datetime NOT NULL," +
                "created_date datetime NOT NULL," +
                "PRIMARY KEY (seq)," +
                "UNIQUE KEY mb_id (mb_id))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS member_token (" +
                "seq int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                "mb_id varchar(255) NOT NULL DEFAULT ''," +
                "notification_token varchar(255) NOT NULL DEFAULT ''," +
                "device_uuid varchar(255) NOT NULL DEFAULT ''," +
                "access_token varchar(255) NOT NULL DEFAULT ''," +
                "access_token_expires datetime NOT NULL," +
                "refresh_token varchar(255) NOT NULL DEFAULT ''," +
                "refresh_token_expires datetime NOT NULL," +
                "KEY mt_key (mb_id,device_uuid)," +
                "KEY device_uuid (device_uuid)," +
                "KEY access_token (access_token)," +
                "KEY refresh_token (refresh_token)," +
                "KEY notification_token (notification_token))");
    }

    private Properties hibProperties() {
        Properties properties = new Properties();
        properties.put(PROPERTY_NAME_HIBERNATE_DIALECT, environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));
        properties.put(PROPERTY_NAME_HIBERNATE_SHOW_SQL, environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));
        properties.put(PROPERTY_NAME_HIBERNATE_ID, false);
        return properties;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }
}
