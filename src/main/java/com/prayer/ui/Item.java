package com.prayer.ui;

public class Item {
    private String menu;
    private String category;

    public Item(String menu, String category) {
        this.menu     = menu;
        this.category = category;
    }

    public String getTitle() {
        return String.format("title.%s", category);
    }

    public String getUrl() {
        return String.format("/%s/%s", menu, category);
    }

    public String getMenu() {
        return menu;
    }

    public String getCategory() {
        return category;
    }
}
