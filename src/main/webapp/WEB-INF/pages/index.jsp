﻿<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en" class="ie ie9">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><spring:message code="title.main"/></title>
    <meta name="keywords" content="prayer"/>
    <meta name="description" content="<spring:message code="title.main"/>"/>
</head>
<body>
<% response.sendRedirect("/swagger-ui.html"); %>
</body>
</html>

